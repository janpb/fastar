/*
 * -------------------------------------------------------------------------------
 * \author Jan Piotr Buchmann <jpb@members.fsf.org>
 * \copyright 2021
 * -------------------------------------------------------------------------------
 */

#include "parser.h"

#include <iostream>

#include "sequence.h"

namespace fasta
{
  Parser::Parser(bool doStore, bool doProcess)
    :doStore(doStore), doProcess(doProcess)
  { }

  Parser::Parser()
    :doStore(false), doProcess(true)
  {  }

  Parser::~Parser()
  {
    std::cerr << "Sequences: " << seqcount << "\nStored: " << sequences.size() << "\n";
  }

  void Parser::parse(fasta::Processor& proc)
  {
    char ch;
    while(std::cin.get(ch))
    {
      switch(parse_status)
      {
        case 0:
          if(ch == '>')
          {
            parse_status = 1;
            break;
          }
        //continue;
        std::cout << "Not valid FASTA data\n";
        return;

        case 1:
          if(ch == '\n')
          {
            parse_status = 2;
            ++linecount;
            break;
          }
          header += ch;
          break;

        case 2:
          if(ch == '\n')
          {
            parse_status = 3;
            ++linecount;
            break;
          }
          if(ch == ' ')
            {continue;}
          seq += ch;
          break;

        case 3:
          if(ch == '>')
          {
            add_sequence(fasta::Sequence(header, seq), proc);
            parse_status = 1;
            break;
          }
          if(ch == '\n')
          {
            ++linecount;
            break;
          }
          seq += ch;
          parse_status = 2;
          break;
      }
    }
    add_sequence(fasta::Sequence(header, seq), proc);
    return; // just in case
  }

  void Parser::add_sequence(fasta::Sequence seq, fasta::Processor& proc)
  {
    if(doStore)
    {
      const auto& [it, pass] = sequences.try_emplace(seq.header(), seq);
      if(!pass)
      {
        std::cout << "Error: line " << linecount - 1
                  << ": duplicate FASTA header ("<< seqcount << "):"
                  << seq.header() <<"\n";
      }
    }
    if(doProcess)
    {
      proc.process(seq);
    }
    ++seqcount;
    reset();
  }

  void Parser::reset()
  {
    header.clear();
    seq.clear();
  }
} // end namespace fasta
