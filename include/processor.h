/*
 * -------------------------------------------------------------------------------
 * \author Jan Piotr Buchmann <jpb@members.fsf.org>
 * \copyright 2021
 * -------------------------------------------------------------------------------
 */

#pragma once

#include <iostream>

#include "sequence.h"

namespace fasta
{
  class Processor
  {
    public:
      virtual void process(fasta::Sequence& seq ) = 0;
  };
} // end namespace fasta
