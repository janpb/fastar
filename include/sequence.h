/*
 * \author Jan Piotr Buchmann <jpb@members.fsf.org>
 * \copyright 2021
 */

#pragma once

#include <iostream>
#include <optional>
#include <vector>

namespace fasta
{
  class Sequence
  {
    public:
      Sequence(std::string header, std::string sequence, const char headerdelim=' ');
      ~Sequence();
      const std::string header(const std::optional<std::string>& delimiter = std::nullopt) const;
      const std::string& sequence() const;
      const std::string accession() const;
      const std::optional<std::string> headertoken(long unsigned int idx) const;
      const std::int_fast32_t length() const;
      void print();

    private:
      std::vector<std::string> headertokens;
      std::string seq;
      void tokenize(const std::string& line);
      const char headerdelim;

  };
} // namespace fasta
