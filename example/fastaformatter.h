/*
 * -------------------------------------------------------------------------------
 * \author Jan Piotr Buchmann <jpb@members.fsf.org>
 * \copyright 2021
 * -------------------------------------------------------------------------------
 */

#pragma once

#include "../include/fasta.h"

#include <iostream>

class FastaFormatter : public fasta::Processor
{
  public:
    FastaFormatter();
    ~FastaFormatter();
    void process(fasta::Sequence& seq);
};
