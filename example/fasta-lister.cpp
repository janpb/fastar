/*
 * \author Jan Piotr Buchmann <jpb@members.fsf.org>
 * \copyright 2021
*/

#include <iostream>

#include "../include/fasta.h"

#include "fastaformatter.h"

int main(int argc, char **argv)
{
  FastaFormatter fmt;
  fasta::Parser p;
  p.parse(fmt);
  return 0;
}
